//
//  RWTFlickrSearchImpl.h
//  RWTFlickrSearch
//
//  Created by crci on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTViewModelServices.h"
#import "RWTFlickrSearchResults.h"
#import "RWTFlickrPhoto.h"
#import <objectiveflickr/ObjectiveFlickr.h>
#import <LinqToObjectiveC/NSArray+LinqExtensions.h>

@interface RWTFlickrSearchImpl : NSObject<RWTFlickrSearch>

@end
