//
//  RWTViewModelServicesImpl.h
//  RWTFlickrSearch
//
//  Created by crci on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTViewModelServices.h"

@interface RWTViewModelServicesImpl : NSObject<RWTViewModelServices>

@end
