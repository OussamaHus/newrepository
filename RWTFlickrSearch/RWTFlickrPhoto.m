//
//  RWTFlickrPhoto.m
//  RWTFlickrSearch
//
//  Created by crci on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickrPhoto.h"

@implementation RWTFlickrPhoto

- (NSString *)description {
    return self.title;
}

@end
