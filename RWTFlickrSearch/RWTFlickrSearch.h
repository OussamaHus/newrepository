//
//  RWTFlickrSearch.h
//  RWTFlickrSearch
//
//  Created by crci on 28/02/2017.
//  Copyright © 2017 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@protocol RWTFlickrSearch <NSObject>

- (RACSignal *)flickrSearchSignal:(NSString *)searchString;

@end
