//
//  Created by Colin Eberhardt on 23/04/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

@import UIKit;
#import "RWTSearchResultsViewModel.h"

@interface RWTSearchResultsViewController : UIViewController

@property (strong, nonatomic) RWTSearchResultsViewModel *viewModel;

- (instancetype)initWithViewModel:(RWTSearchResultsViewModel *)viewModel;

@end
